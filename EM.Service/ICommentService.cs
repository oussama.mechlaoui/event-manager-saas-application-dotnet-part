﻿using EM.Domain.Entities;
using Service.Pattern;
using System.Collections.Generic;

namespace EM.Service
{
    public interface ICommentService : IService<Comment>
    {
        IEnumerable<Comment> SearchCommentsByName(string searchString);

        //Appel de la fonction de CommentService(WebService)
        List<Comment> getMandates();
    }
}

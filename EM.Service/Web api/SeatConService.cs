﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Service.Web_api
{
    public class SeatConService : Service<Seat>, ISeatConService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public SeatConService() : base(utk)
        {
        }


        public List<Double> TicketSum()
        {


            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();


            {
                Double t = 0;


                string vpSelect = "select sum(prixTicket) FROM Tickets where TicketType = 0  GROUP BY (TicketType)  ";
                SqlCommand cmdvp = new SqlCommand(vpSelect, con);
                SqlDataReader myReadervp = cmdvp.ExecuteReader();
                while (myReadervp.Read())
                {
                    // Assuming your desired value is the name as the 3rd field
                    Console.WriteLine("TEST = " + myReadervp.GetDouble(0));
                    t = myReadervp.GetDouble(0);

                }
                myReadervp.Close();

                
                ////////////////
                Double q = 0;


                string nrSelect = "select sum(prixTicket) FROM Tickets where TicketType = 1  GROUP BY (TicketType) ";
                SqlCommand cmdnr = new SqlCommand(nrSelect, con);
                SqlDataReader myReadernr = cmdnr.ExecuteReader();
                while (myReadernr.Read())
                {
                    // Assuming your desired value is the name as the 3rd field
                    Console.WriteLine("TEST = " + myReadernr.GetDouble(0));
                    q = myReadernr.GetDouble(0);

                }
                myReadernr.Close();
                List<Double> TicketSum = new List<Double>();
                TicketSum.Add(t);
                TicketSum.Add(q);
                return TicketSum;

            }
        }
    }
}
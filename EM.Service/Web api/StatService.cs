﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Data.Configurations;
using System.Configuration;

namespace EM.Service.Web_api
{

    public class StatService : Service<Ticket>, IStatService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public StatService() : base(utk)
        {
        }

        
        public List<int> SommePrixTicket()
        {


            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();


            int i = 0;

            string strSelect = "Select count(*) From Tickets ";
            SqlCommand cmd = new SqlCommand(strSelect, con);
            SqlDataReader myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReader.GetInt32(0));
                i = myReader.GetInt32(0);
            }
            myReader.Close();
            List<int> somTicket = new List<int>();
            somTicket.Add(i);
            return somTicket;
        }

    }
}


﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Service.Web_api
{
    public class KService : Service<Ticket>, IKService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public KService() : base(utk)
        {
        }


        public ArrayList StatusTicket()
        {


            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();

            ArrayList list = new ArrayList(new[] { 0, 0, 0, 0 });
           
            /////////////////////////
            Double yea = 0;
            string thisyearSelect = "select year(DateLancementSales) , sum(prixTicket) from Tickets  GROUP BY year(DateLancementSales) ";
            SqlCommand cmdyear = new SqlCommand(thisyearSelect, con);
            SqlDataReader myReaderyear = cmdyear.ExecuteReader();
            while (myReaderyear.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderyear.GetInt32(0));
                yea = myReaderyear.GetDouble(1);
                if (myReaderyear.GetInt32(0) == 2016)
                {
                    list[0] = yea;

                }
               
                else if (myReaderyear.GetInt32(0) == 2017) { list[1] = yea; }
                else if (myReaderyear.GetInt32(0) == 2018) { list[2] = yea; }
                else if (myReaderyear.GetInt32(0) == 2019) { list[3] = yea; }
              
            }
            myReaderyear.Close();
           
            
            
            return list;
        }
    }
}
﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Service.Web_api
{
   public class SeatService : Service<Seat>, ISeatService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public SeatService() : base(utk)
        {

        }
        public List<Seat> getMandatess()
        {
            IEnumerable<Seat> m = (from complaints in utk.GetRepositoryBase<Seat>().GetAll()
                                   select complaints);
            List<Seat> list = m.ToList<Seat>();
            return list;
        }
    }
}
﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Service.Web_api
{
    public class SService : Service<Seat>, ISService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public SService() : base(utk)
        {
        }


        public List<int> TypeSeat()
        {


            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();
            {
                int d = 0;
                string soldSelect = "Select count(*) From Seats where SeatStatus = 0";
                SqlCommand cmdsold = new SqlCommand(soldSelect, con);
                SqlDataReader myReadersold = cmdsold.ExecuteReader();
                while (myReadersold.Read())
                {
                    // Assuming your desired value is the name as the 3rd field
                    Console.WriteLine("TEST = " + myReadersold.GetInt32(0));
                    d = myReadersold.GetInt32(0);
                }
                myReadersold.Close();


                //////////////////
                int o = 0;
                string unsoldSelect = "Select count(*) From Seats where SeatStatus = 1";
                SqlCommand cmdunsold = new SqlCommand(unsoldSelect, con);
                SqlDataReader myReaderunsold = cmdunsold.ExecuteReader();
                while (myReaderunsold.Read())
                {
                    // Assuming your desired value is the name as the 3rd field
                    Console.WriteLine("TEST = " + myReaderunsold.GetInt32(0));
                    o = myReaderunsold.GetInt32(0);
                }
                myReaderunsold.Close();
                List<int> TypeSeat = new List<int>();
                TypeSeat.Add(d);
                TypeSeat.Add(o);
                return TypeSeat;
            }
        }
    }
}
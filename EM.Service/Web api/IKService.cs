﻿using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Service.Web_api
{
   public interface IKService : IService<Ticket>, IDisposable
    {
    }
}

﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EM.Service.Web_api
{
    public class TService : Service<Ticket>, ITService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base



        public TService() : base(utk)
        {
        }


        public List<int> TypeTicket()
        {

            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();



            int n = 0;
            string normlSelect = "Select count(*) From Tickets where TicketType =1 ";
            SqlCommand cmdnorml = new SqlCommand(normlSelect, con);
            SqlDataReader myReadernorml = cmdnorml.ExecuteReader();
            while (myReadernorml.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadernorml.GetInt32(0));
                n = myReadernorml.GetInt32(0);
            }
            myReadernorml.Close();


            //

            int k = 0;
            string vipSelect = "Select count(*) From Tickets where TicketType =0 ";
            SqlCommand cmdvip = new SqlCommand(vipSelect, con);
            SqlDataReader myReadervip = cmdvip.ExecuteReader();
            while (myReadervip.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadervip.GetInt32(0));
                k = myReadervip.GetInt32(0);
            }
            myReadervip.Close();

            List<int> tyetick = new List<int>();
            tyetick.Add(n);
            tyetick.Add(k);
            return tyetick;



        }
    }
}
﻿using EM.Data.Infrastructure;
using Service.Pattern;
using EM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Service;


namespace EM.Service
{
    public class serviceTasks : Service<Tasks>, IserviceTasks
    {
        static IDataBaseFactory dbf = new DataBaseFactory();

        static IUnitOfWork uow = new UnitOfWork(dbf);

        public serviceTasks() : base(uow)
        {
        }
    }
}

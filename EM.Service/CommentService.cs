﻿using EM.Data.Infrastructure;
using EM.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EM.Service
{
    public class CommentService : Service<Comment>, ICommentService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();//l'usine de fabrication du context
        static IUnitOfWork utk = new UnitOfWork(Factory);//unité de travail a besoin du factory pour communiquer avec la base
        public CommentService() : base(utk)
        {

        }




        // Fonction Recherche 
        public IEnumerable<Comment> SearchCommentsByName(string searchString)
        {

            IEnumerable<Comment> CommentsDomain = GetMany();
            if (!String.IsNullOrEmpty(searchString))
            {
                CommentsDomain = GetMany(x => x.ParticipantName.Contains(searchString));
            }
            return CommentsDomain;
        }


        //en relation avec le WebService

        public List<Comment> getMandates()
        {
            IEnumerable<Comment> m = (from cts in utk.GetRepositoryBase<Comment>().GetAll()
                                      select cts);
            List<Comment> list = m.ToList<Comment>();
            return list;
        }
    }
}

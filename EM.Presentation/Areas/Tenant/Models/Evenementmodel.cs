﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EM.Presentation.Areas.Tenant.Models
{
    public class Evenementmodel
    {
        [Key]
        public int EventId { get; set; }
        [Required]
        public string TenantId { get; set; }
        [Required(ErrorMessage = "The event name must be filled")]
        public string Name { get; set; }
        [Required(ErrorMessage = "The event Picture must be added")]
        public string Picture { get; set; }
        [Required(ErrorMessage = "The event theme must be filled")]
        public string Theme { get; set; }
        [Required(ErrorMessage = "The event location must be filled")]
        public string Location { get; set; }
        [Required(ErrorMessage = "The event Start Date must be filled")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "The event End Date must be filled")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        //public virtual ICollection<Tasks> Tasks { get; set; }
    }
}
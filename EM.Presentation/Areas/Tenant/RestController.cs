﻿using EM.Presentation.Areas.Tenant.Controllers;
using EM.Service.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Areas.Tenant
{
    public class RestController : ApiController
    {

        public IUserService UserService;
        private ApplicationUserManager _userManager;
        private ApplicationUserManagerApi _userManagerApi;
        private ApplicationRoleManager _roleManager = null;
        private ApplicationSignInManager _signInManager;
        private ModelFactory _modelFactory;

        public RestController()
        {
            UserService = new UserService();
        }
        public RestController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationUserManagerApi userManagerApi) : base()
        {
            UserManager = userManager;
            UserManagerApi = userManagerApi;
            SignInManager = signInManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationUserManagerApi UserManagerApi
        {
            get
            {
                return _userManagerApi?? Request.GetOwinContext().GetUserManager<ApplicationUserManagerApi>();
            }
            private set
            {
                _userManagerApi = value;
            }
        }

        protected ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.UserManagerApi);
                }
                return _modelFactory;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }


        public EM.Domain.Entities.Tenant current_tenant
        {
            get
            {
                object apptenant;
                if (!Request.GetOwinContext().Environment.TryGetValue("Tenant", out apptenant))
                {
                    return null;
                }
                return (EM.Domain.Entities.Tenant)apptenant;
            }

        }

         
    }
}

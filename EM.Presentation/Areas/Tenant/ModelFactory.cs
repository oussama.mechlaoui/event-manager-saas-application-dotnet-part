﻿using EM.Data;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;

namespace EM.Presentation.Areas.Tenant
{
    public class ModelFactory
    {
        private UrlHelper _UrlHelper;
        private ApplicationUserManagerApi _AppUserManager;

        public ModelFactory(HttpRequestMessage request, ApplicationUserManagerApi appUserManager)
        {
            _UrlHelper = new UrlHelper(request);
            _AppUserManager = appUserManager;

        }

        public UserReturnModel Create(ApplicationUser appUser)
        {
            if (appUser.Tenant != null)
            {
                return new UserReturnModel
                {
                    //Url = _UrlHelper.Link("GetUserById", new { id = appUser.Id }),
                    Id = appUser.Id,
                    TenantId = appUser.TenantId,
                    TenantName = appUser.Tenant.TenantName,
                    UserName = appUser.UserName,
                    Email = appUser.Email,
                    Role = _AppUserManager.GetRolesAsync(appUser.Id).Result.ToList().FirstOrDefault(),
                    Status = _AppUserManager.GetClaimsAsync(appUser.Id).Result.ToList().Find(c => c.Type.Equals("Active")).Value
                };
            }
            else
            {
                return new UserReturnModel
                {
                    //Url = _UrlHelper.Link("GetUserById", new { id = appUser.Id }),
                    Id = appUser.Id,
                    UserName = appUser.UserName,
                    Email = appUser.Email,
                    Role = _AppUserManager.GetRolesAsync(appUser.Id).Result.ToList().FirstOrDefault(),
                    Status = _AppUserManager.GetClaimsAsync(appUser.Id).Result.ToList().Find(c => c.Type.Equals("Active")).Value
                };
            }
        }
    }

    public class UserReturnModel
    {
        //public string Url { get; set; }
        public string Id { get; set; }

        public int? TenantId { get; set; }
        public string TenantName { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        //public bool EmailConfirmed { get; set; }
        public String Role { get; set; }
        public String Status { get; set; }
        //public IList<System.Security.Claims.Claim> Claims { get; set; }
    }
}
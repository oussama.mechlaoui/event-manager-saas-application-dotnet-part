﻿using EM.Domain.Entities;
using EM.Presentation.Areas.Tenant.Models;
using EM.Service;
using EM.Service.UserService;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EM.Presentation.Areas.Tenant.Controllers
{
    public class TasksController : AppController
    {
        IUserService UserService;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private EM.Domain.Entities.Tenant _tenant;
        Evenement_service ss = new Evenement_service();
        serviceTasks sa = new serviceTasks();


        public TasksController()
        {
            UserService = new UserService();
        }
        public TasksController(ApplicationUserManager userManager, ApplicationRoleManager roleManager):base()
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public EM.Domain.Entities.Tenant Current_Tenant
        {
            get
            {
                return _tenant ?? base.current_tenant;
            }
            private set
            {
                _tenant = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        /********* Actions Starts Heres *******/


        public ActionResult Index()
        {
            var tasks = sa.GetMany(null, null, t => t.User);
            foreach (EM.Domain.Entities.Tasks t in tasks)
            {
                var vevent = new Evenement();
                vevent = ss.GetById(t.EventId);
                t.Evenement = vevent; 
            }
            return View(tasks);
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            } */
            Tasks conge = sa.GetById(id);
            if (conge == null)
            {
                return HttpNotFound();
            }
            return View(conge);
            ////////////////////////////////////////////////////////

        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            var cat = UserService.GetMany(u => u.TenantId == Current_Tenant.TenantId);
            var cad = ss.GetMany();
            tasksmodel pm = new tasksmodel();
            pm.users = cat.Select(p => new SelectListItem
            {
                Text = p.UserName,
                Value = p.Id
            });
            pm.Evenements = cad.Select(y => new SelectListItem
            {
                Text = y.Theme,
                Value = y.EventId.ToString()
            });

            return View(pm);
        }

        // POST: Tasks/Create
        [HttpPost]
        public ActionResult Create(tasksmodel pm)
        {
            Tasks p = new Tasks();
            p.type = pm.type;
            p.Id = pm.Id;
            p.EventId = pm.EventId;
            //2eme etape
            //prods.Add(p);
            //3eme etape
            //Session["Products"] = prods;
            sa.Add(p);
            sa.Commit();
            //enregistrer l'image

            return RedirectToAction("Index");
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int id)
        {
            Tasks p = sa.GetById(id);

            //    Domain.Entities.employe cat = se.GetById(id);

            tasksmodel pm = new tasksmodel();


            var congeModel = new tasksmodel
            {
                idTasks = p.idTasks,
                type = p.type,
                EventId = p.EventId,
                User = p.User


            };

            var emp = UserService.GetMany();
            var emp1 = ss.GetMany();

            tasksmodel pm1 = new tasksmodel();

            pm1.users = emp.Select(pp => new SelectListItem
            { Text = pp.UserName, Value = pp.Id.ToString() });

            congeModel.users = pm1.users;

            pm1.Evenements = emp1.Select(pp => new SelectListItem
            { Text = pp.Theme, Value = pp.EventId.ToString() });

            congeModel.Evenements = pm1.Evenements;

            return View(congeModel);

        }

        // POST: Tasks/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tasksmodel congemodel)
        {
            try
            {
                var conge = sa.GetMany().Where(c => c.idTasks.Equals(id)).FirstOrDefault();

                // TODO: Add update logic here
                conge.idTasks = congemodel.idTasks;
                conge.type = congemodel.type;
                conge.User.Id = congemodel.User.Id;
                conge.EventId = congemodel.EventId;




                sa.Update(conge);
                Evenement e = new Evenement();
                var emp = ss.GetMany().Where(c => c.EventId.Equals(congemodel.EventId)).FirstOrDefault();


                sa.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int id)
        {
            var Conge = sa.GetMany().Where(c => c.idTasks.Equals(id)).FirstOrDefault();
            sa.Delete(Conge);
            sa.Commit();
            return RedirectToAction("Index");
        }

        // POST: Tasks/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var Conge = sa.GetMany().Where(c => c.idTasks.Equals(id)).FirstOrDefault();
                sa.Delete(Conge);
                sa.Commit();
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

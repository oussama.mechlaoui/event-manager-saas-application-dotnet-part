﻿using EM.Domain.Entities;
using EM.Presentation.Areas.Tenant.Models;
using EM.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Areas.Tenant.Controllers
{
    public class EventWebApiController : ApiController
    {

        Evenement_service MyService;


        private Evenement_service ps = new Evenement_service();
        
        public EventWebApiController()
        {
            MyService = new Evenement_service();
            
        }

        //public List<Evenementmodel> Index()
        //{
        //    List<Evenement> events = ps.GetMany().ToList();
        //    List<Evenementmodel> pubXml = new List<Evenementmodel>();
        //    foreach (Evenement p in events)
        //    {
        //        pubXml.Add(new Evenementmodel
        //        {
        //            EndDate=p.EndDate,
        //            EventId=p.EventId,
        //            Location=p.Location,
        //            Name=p.Name,
        //            Picture=p.Picture,
        //            StartDate=p.StartDate,
        //            Theme=p.Theme


        //        });
        //    }
        //    return pubXml;
        //}

        // GET api/<controller>
        public List<Evenementmodel> Get()
        {
            List<Evenement> events = ps.GetMany().ToList();
            List<Evenementmodel> pubXml = new List<Evenementmodel>();
            foreach (Evenement p in events)
            {
                pubXml.Add(new Evenementmodel
                {

                    EndDate = p.EndDate,
                    EventId = p.EventId,
                    Location = p.Location,
                    Name = p.Name,
                    Picture = p.Picture,
                    StartDate = p.StartDate,
                    Theme = p.Theme


                });
            }
            return pubXml;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]

        [Route("api/Create")]
        public Evenement Post(Evenement e)
        {
            
            MyService.Add(e);
            MyService.Commit();
            return e;
        }


        // PUT api/<controller>/5
        [HttpPut]

        [Route("api/Update")]
        public Evenement Put(int id, Evenement e)
        {
            Evenement p = MyService.GetById(id);
            p.EndDate = e.EndDate;
            p.Location = e.Location;
            p.Name = e.Name;
            p.Picture = e.Picture;
            p.StartDate = e.StartDate;
            p.Theme = e.Theme;
            
            
            MyService.Update(p);
            MyService.Commit();
            return p;

        }

        
        
        public IHttpActionResult Delete(int id)
        {
            Evenement comp = MyService.GetById(id);

            MyService.Delete(comp);
            MyService.Commit();

            return Ok(comp);
        }
    }
}
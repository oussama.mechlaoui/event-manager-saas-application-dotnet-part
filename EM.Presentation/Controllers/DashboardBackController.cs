﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Presentation.Controllers
{
    public class DashboardBackController : Controller
    {
        // GET: DashboardBack
        public ActionResult Index()
        {





            return View();
        }

       

        public ActionResult SecondAjax()
        {



            int i = 0;

            string conn = ConfigurationManager.ConnectionStrings["EM_Remote_DB"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            con.Open();




            string strSelect = "Select count(*) From Tickets ";
            SqlCommand cmd = new SqlCommand(strSelect, con);
            SqlDataReader myReader = cmd.ExecuteReader();
            while (myReader.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReader.GetInt32(0));
                i = myReader.GetInt32(0);
            }
            myReader.Close();
            
            ViewBag.total = i;


            //////
            int k = 0;
            string vipSelect = "Select count(*) From Tickets where TicketType =0 ";
            SqlCommand cmdvip = new SqlCommand(vipSelect, con);
            SqlDataReader myReadervip = cmdvip.ExecuteReader();
            while (myReadervip.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadervip.GetInt32(0));
                k = myReadervip.GetInt32(0);
            }
            myReadervip.Close();
            
            ViewBag.ticketVip = k;

            //////
            int n = 0;
            string normlSelect = "Select count(*) From Tickets where TicketType =1 ";
            SqlCommand cmdnorml = new SqlCommand(normlSelect, con);
            SqlDataReader myReadernorml = cmdnorml.ExecuteReader();
            while (myReadernorml.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadernorml.GetInt32(0));
                n = myReadernorml.GetInt32(0);
            }
            myReadernorml.Close();
            
            ViewBag.ticketNorml = n;

            //////////////
            Double s = 0;
            ArrayList RevnByMonth = new ArrayList(new[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

            string RevnSelect = "select MONTH(DateLancementSales),year(DateLancementSales) , sum(prixTicket) FROM Tickets where MONTH(DateLancementSales)=MONTH(GETDATE()) and  year(DateLancementSales)=year(GETDATE())  GROUP BY month(DateLancementSales), year(DateLancementSales) ORDER BY month(DateLancementSales), year(DateLancementSales) ASC ";
            SqlCommand cmdrvn = new SqlCommand(RevnSelect, con);
            SqlDataReader myReadervn = cmdrvn.ExecuteReader();
            while (myReadervn.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadervn.GetDouble(2));
                s = myReadervn.GetDouble(2);

                if (myReadervn.GetInt32(1) == DateTime.Now.Year)
                {
                    if (myReadervn.GetInt32(0) == 1) { RevnByMonth[0] = s; }
                    if (myReadervn.GetInt32(0) == 2) { RevnByMonth[1] = s; }
                    if (myReadervn.GetInt32(0) == 3) { RevnByMonth[2] = s; }
                    if (myReadervn.GetInt32(0) == 4) { RevnByMonth[3] = s; }
                    if (myReadervn.GetInt32(0) == 5) { RevnByMonth[4] = s; }
                    if (myReadervn.GetInt32(0) == 6) { RevnByMonth[5] = s; }
                    if (myReadervn.GetInt32(0) == 7) { RevnByMonth[6] = s; }
                    if (myReadervn.GetInt32(0) == 8) { RevnByMonth[7] = s; }
                    if (myReadervn.GetInt32(0) == 9) { RevnByMonth[8] = s; }
                    if (myReadervn.GetInt32(0) == 10) { RevnByMonth[9] = s; }
                    if (myReadervn.GetInt32(0) == 11) { RevnByMonth[10] = s; }
                    if (myReadervn.GetInt32(0) == 12) { RevnByMonth[11] = s; }

                }








            }
            myReadervn.Close();
           
            ViewBag.TicketRvn = RevnByMonth;

            ////////
            Double yea = 0;
            string thisyearSelect = "select year(DateLancementSales) , sum(prixTicket) from Tickets where year(DateLancementSales) = year (GETDATE()) GROUP BY year(DateLancementSales) ";
            SqlCommand cmdyear = new SqlCommand(thisyearSelect, con);
            SqlDataReader myReaderyear = cmdyear.ExecuteReader();
            while (myReaderyear.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderyear.GetDouble(1));
                yea = myReaderyear.GetDouble(1);
            }
            myReaderyear.Close();

            ViewBag.prixyear = yea;
            ////////////////////
            int c = 0;
            string seatSelect = "Select count(*) From Seats";
            SqlCommand cmdseat = new SqlCommand(seatSelect, con);
            SqlDataReader myReaderseat = cmdseat.ExecuteReader();
            while (myReaderseat.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderseat.GetInt32(0));
                c = myReaderseat.GetInt32(0);
            }
            myReaderseat.Close();

            ViewBag.prixseat = c;
            ///////////////
            int d = 0;
            string soldSelect = "Select count(*) From Seats where SeatStatus = 0";
            SqlCommand cmdsold = new SqlCommand(soldSelect, con);
            SqlDataReader myReadersold = cmdsold.ExecuteReader();
            while (myReadersold.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadersold.GetInt32(0));
                d = myReadersold.GetInt32(0);
            }
            myReadersold.Close();

            ViewBag.prixsold = d;
            //////////////////
            int o = 0;
            string unsoldSelect = "Select count(*) From Seats where SeatStatus = 1";
            SqlCommand cmdunsold = new SqlCommand(unsoldSelect, con);
            SqlDataReader myReaderunsold = cmdunsold.ExecuteReader();
            while (myReaderunsold.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderunsold.GetInt32(0));
                o = myReaderunsold.GetInt32(0);
            }
            myReaderunsold.Close();

            ViewBag.prixunsold = o;

            /////////////////
            int e = 0;
            string emptySelect = "Select count(*) From Seats where SeatType = 1";
            SqlCommand cmdempty = new SqlCommand(emptySelect, con);
            SqlDataReader myReaderempty = cmdempty.ExecuteReader();
            while (myReaderempty.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderempty.GetInt32(0));
                e = myReaderempty.GetInt32(0);
            }
            myReaderempty.Close();

            ViewBag.prixempty = e;

            //////////////////////////
            Double t = 0;


            string vpSelect = "select sum(prixTicket) FROM Tickets where TicketType = 0  GROUP BY (TicketType)  ";
            SqlCommand cmdvp = new SqlCommand(vpSelect, con);
            SqlDataReader myReadervp = cmdvp.ExecuteReader();
            while (myReadervp.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadervp.GetDouble(0));
                t = myReadervp.GetDouble(0);

            }
            myReadervp.Close();

            ViewBag.prixvp = t;
            ////////////////
            Double q = 0;


            string nrSelect = "select sum(prixTicket) FROM Tickets where TicketType = 1  GROUP BY (TicketType) ";
            SqlCommand cmdnr = new SqlCommand(nrSelect, con);
            SqlDataReader myReadernr = cmdnr.ExecuteReader();
            while (myReadernr.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReadernr.GetDouble(0));
                q = myReadernr.GetDouble(0);

            }
            myReadernr.Close();

            ViewBag.prixnr = q;

            /////////////
         /*   Double w = 0;
            ArrayList ByMonth = new ArrayList(new[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

            string ReSelect = "select MONTH(DateLancementSales), sum(prixTicket) FROM Tickets where Ticketonline = 0  GROUP BY (Ticketonline), month(DateLancementSales)";
            SqlCommand cmdrv = new SqlCommand(ReSelect, con);
            SqlDataReader myReaderv = cmdrv.ExecuteReader();
            while (myReaderv.Read())
            {
                // Assuming your desired value is the name as the 3rd field
                Console.WriteLine("TEST = " + myReaderv.GetDouble(2));
                w = myReaderv.GetDouble(2);

                if (myReaderv.GetInt32(0) == DateTime.Now.Year)
                {
                    if (myReaderv.GetInt32(0) == 1) { ByMonth[0] = w; }
                    if (myReaderv.GetInt32(0) == 2) { ByMonth[1] = w; }
                    if (myReaderv.GetInt32(0) == 3) { ByMonth[2] = w; }
                    if (myReaderv.GetInt32(0) == 4) { ByMonth[3] = w; }
                    if (myReaderv.GetInt32(0) == 5) { ByMonth[4] = w; }
                    if (myReaderv.GetInt32(0) == 6) { ByMonth[5] = w; }
                    if (myReaderv.GetInt32(0) == 7) { ByMonth[6] = w; }
                    if (myReaderv.GetInt32(0) == 8) { ByMonth[7] = w; }
                    if (myReaderv.GetInt32(0) == 9) { ByMonth[8] = w; }
                    if (myReaderv.GetInt32(0) == 10) { ByMonth[9] = w; }
                    if (myReaderv.GetInt32(0) == 11) { ByMonth[10] = w; }
                    if (myReaderv.GetInt32(0) == 12) { ByMonth[11] = w; }

                }

            }
            myReaderv.Close();

            ViewBag.TicketRv = ByMonth;





    */









            ArrayList list = new ArrayList();
            list.Add(i);
            list.Add(k);
            list.Add(RevnByMonth);
            list.Add(n);
            list.Add(yea);
            list.Add(c);
            list.Add(d);
            list.Add(o);
            list.Add(e);
           list.Add(t);
            list.Add(q);
       //   list.Add(ByMonth);





            return Json(list, JsonRequestBehavior.AllowGet);
        }


      

       





















    }
}
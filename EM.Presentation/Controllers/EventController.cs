﻿using EM.Data;
using EM.Domain.Entities;
using EM.Presentation.Areas.Tenant;
using EM.Presentation.Models;
using EM.Service;
using EM.Service.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EM.Presentation.Controllers
{
    public class EventController : AppController
    {
        IUserService UserService;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private EM.Domain.Entities.Tenant _tenant;
        ICommentService MyCommentService;

        Evenement_service aa = new Evenement_service();
        public EventController()
        {
            UserService = new UserService();
            MyCommentService = new CommentService();
        }
        public EventController(ApplicationUserManager userManager, ApplicationRoleManager roleManager):base()
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public EM.Domain.Entities.Tenant Current_Tenant
        {
            get
            {
                return _tenant ?? base.current_tenant;
            }
            private set
            {
                _tenant = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }



        // GET: Tenant/Evenement
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Index()
        {

            
            var events = aa.GetMany(null, null, t => t.Tenant);
            
                return View(events);
            
        }

        // GET: EventBack/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evenement conge = aa.GetById(id);
            if (conge == null)
            {
                return HttpNotFound();
            }

            var Comments = new List<CommentVM>();
            ApplicationUser current_user = UserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            foreach (Comment c in MyCommentService.GetMany(c => c.EventId == conge.EventId))
            {
                var participant = UserManager.FindById(c.ParticipantId);
                Comments.Add(new CommentVM()
                {
                    Id = c.Id,
                    Text = c.Text,
                    CreatedOn = c.CreatedOn,
                    ParticipantId = c.ParticipantId

                });
                ViewData["comments"] = Comments;                
            }
            return View(conge);
        }

        [HttpPost]
        public ActionResult Details(int? id, string contenu)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evenement conge = aa.GetById(id.GetValueOrDefault());
            if (conge == null)
            {
                return HttpNotFound();
            }

            var Comments = new List<CommentVM>();
            ApplicationUser current_user = UserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            foreach (Comment c in MyCommentService.GetMany(c => c.EventId == conge.EventId))
            {
                var participant = UserManager.FindById(c.ParticipantId);
                Comments.Add(new CommentVM()
                {
                    Id = c.Id,
                    Text = c.Text,
                    CreatedOn = c.CreatedOn,
                    ParticipantId = c.ParticipantId

                });
                ViewData["comments"] = Comments;
            }

            //Comment creation
            if (contenu != null)
            {

                Comment CommentDoamin = new Comment()
                {

                    Text = contenu,
                    CreatedOn = DateTime.UtcNow,
                    ParticipantId = current_user.Id,
                    ParticipantName = current_user.UserName,
                    EventId = conge.EventId
                    //ParticipantImage = ParticipantImage.FileName,

                };
                MyCommentService.Add(CommentDoamin);
                MyCommentService.Commit();



                return PartialView("~/Views/Comment/OneComment.cshtml", new CommentVM
                {
                    Text = CommentDoamin.Text,
                    CreatedOn = CommentDoamin.CreatedOn,
                    ParticipantId = CommentDoamin.ParticipantId,
                    ParticipantName = CommentDoamin.ParticipantName,
                    ParticipantImage = CommentDoamin.ParticipantImage,
                });

            }
            return View(conge);
        }

        // GET: EventBack/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EventBack/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EventBack/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EventBack/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EventBack/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EventBack/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

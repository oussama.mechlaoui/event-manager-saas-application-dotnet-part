﻿using EM.Domain.Entities;
using EM.Presentation.Models;
using EM.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Controllers
{
    public class ReplyWebApiController : ApiController
    {
        IReplyService MyService1 = null;


        private ReplyService ms = new ReplyService();
        List<ReplyVM> reclams1 = new List<ReplyVM>();
        public ReplyWebApiController()
        {
            MyService1 = new ReplyService();
            Index1();
            reclams1 = Index1().ToList();
        }


        public List<ReplyVM> Index1()
        {
            List<Reply> mandates = ms.getMandates1();
            List<ReplyVM> mandatesXml = new List<ReplyVM>();
            foreach (Reply r in mandates)
            {
                mandatesXml.Add(new ReplyVM
                {
                    Id = r.Id,
                    Text = r.Text,
                    CreatedOn = r.CreatedOn,

                });
            }
            return mandatesXml;
        }
        public IEnumerable<ReplyVM> Get()
        {
            return reclams1;
        }

        // GET: api/RecWebApi/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]

        [Route("api/Create1")]
        public Reply Post(Reply rep)
        {
            rep.CreatedOn = DateTime.UtcNow;
            MyService1.Add(rep);
            MyService1.Commit();
            return rep;
        }

        [HttpPut]
        [Route("api/Update")]

        public void Put(int id, Reply loo)
        {
            Reply rep = MyService1.GetById(id);
            rep.Text = loo.Text;
        }



        public IHttpActionResult Delete(int id)

        {
            Reply rep = MyService1.GetById(id);

            MyService1.Delete(rep);
            MyService1.Commit();

            return Ok(rep);


        }



    }





}
      


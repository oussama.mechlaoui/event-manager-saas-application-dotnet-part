﻿using EM.Data;
using EM.Presentation.Areas.Tenant;
using EM.Presentation.Areas.Tenant.Helpers;
using EM.Presentation.Areas.Tenant.Models;
using EM.Presentation.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.Presentation.Controllers.API
{
    [RoutePrefix("api/tenant")]
    public class TenantController : RestController
    {

        [Authorize(Roles = "Administrator")]
        //[AllowAnonymous]
        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            return Ok(this.UserService.GetMany(null, null, u => u.Tenant).ToList().Select(u => this.TheModelFactory.Create(u)));
        }

        [Authorize(Roles ="President")]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string Id)
        {
            var user = await UserManager.FindByIdAsync(Id);
                                                        

            //user.TenantId = this.current_tenant.TenantId;

            if (user != null)
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            return NotFound();

        }

        [Authorize]
        [Route("user/{email}")]
        public async Task<IHttpActionResult> GetUserByName(string email)
        {
            var user = await this.UserManager.FindByEmailAsync(email);


            if (user != null)
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            return NotFound();

        }
        [AllowAnonymous]
        [Route("create")]
        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser();

            if (createUserModel.TenantName != null)
            {
                user.Email = createUserModel.Email;
                user.UserName = createUserModel.Email;
                user.Tenant = new EM.Domain.Entities.Tenant() { TenantName = createUserModel.TenantName };

            }
            else
            {
                user.Email = createUserModel.Email;
                user.UserName = createUserModel.Email;
            }

            IdentityResult addUserResult = await this.UserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

                        if(user.Tenant!= null)
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("Active", "pending"));
                await UserManager.AddToRoleAsync(user.Id, RoleNames.ROLE_President);
            }
            else
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("Active", "actived"));
                await UserManager.AddToRoleAsync(user.Id, RoleNames.ROLE_Participant);
            }
            

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, TheModelFactory.Create(user));
        }

        [Authorize(Roles = "President,Organizer")]
        [Route("changepass")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await this.UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Authorize(Roles = "Administrator")]
        [Route("reject/{id:guid}")]
        public async Task<IHttpActionResult> RejectTenant(string id)
        {
 
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
 
            var appUser = await this.UserManagerApi.FindByIdAsync(id);
 
            if (appUser != null)
            {
                IdentityResult result = await this.UserManagerApi.DeleteAsync(appUser);
 
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
 
                return Ok();
 
            }
 
            return NotFound();
          
        }
        [Authorize(Roles = "Administrator")]
        [Route("activation/{id:guid}")]
        public async Task<IHttpActionResult> ValidateTenant(string id)
        {
            ApplicationUser user = UserService.GetById(id) as ApplicationUser;
            if (user != null)
            {
                var claim = user.Claims.FirstOrDefault(c => c.ClaimType.Equals("Active"));
                user.Claims.Remove(claim);
                UserManagerApi.AddClaim(user.Id, new System.Security.Claims.Claim("Active", "actived"));
                UserService.Update(user);
                UserService.Commit();
                return Ok();
            }
            return NotFound();
            
        }

    }
}

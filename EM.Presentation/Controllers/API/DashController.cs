﻿using EM.Service.Web_api;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Collections;
using System.Linq;
using EM.Domain.Entities;
using EM.Presentation.Models;

namespace EM.Presentation.Controllers.API
{
    public class DashController : ApiController
    {
       ISeatService MyService = null;
       

        private SeatService ms = new SeatService();
        List<EM.Presentation.Models.SeatViewModels.Seat> reclams = new List<EM.Presentation.Models.SeatViewModels.Seat>();
        public DashController()
        {
            MyService = new SeatService();
            Index();
            reclams = Index().ToList();
        }


        public List<EM.Presentation.Models.SeatViewModels.Seat> Index()
        {
            List<Seat> mandates = ms.getMandatess();
            List<EM.Presentation.Models.SeatViewModels.Seat> mandatesXml = new List<EM.Presentation.Models.SeatViewModels.Seat>();
            foreach (Seat i in mandates)
            {
                mandatesXml.Add(new EM.Presentation.Models.SeatViewModels.Seat
                {
                    idSeat = i.idSeat,
                    prixSeat = i.prixSeat,
                    Evennement = i.Evennement,
                    EventId = i.EventId,

                });
            }
            return mandatesXml;
        }

        // GET: api/RecWebApi
        [Route("api/Seat")]
        public IEnumerable<EM.Presentation.Models.SeatViewModels.Seat> Get()
        {
            return reclams;
        }




        public Seat Get(int id)
        {
            Seat comp = MyService.GetById(id);

            return comp;
        }




        // GET: api/RecWebApi/5
        /**   public string Get(int? id)
           {
               return null;
           }
       **/
        // POST: api/RecWebApi



        public EM.Presentation.Models.SeatViewModels.Seat registerStudent(Seat studentregd)
        {
            SeatViewModels.Seat stdregreply = new EM.Presentation.Models.SeatViewModels.Seat();
            MyService.Add(studentregd);


            stdregreply.prixSeat = studentregd.prixSeat;

            stdregreply.Evennement = studentregd.Evennement;



            return stdregreply;
        }

        [HttpPost]
        [Route("api/ajout")]
        public Seat Post(Seat proj)
        {


            MyService.Add(proj);
            MyService.Commit();

            return proj;
        }


        // PUT: api/RecWebApi/5
        [HttpPut]
        [Route("api/modif")]
        public void Put(int id, Seat studentregd)
        {
            Seat stdregreply = MyService.GetById(id);
            stdregreply.prixSeat = studentregd.prixSeat;
            stdregreply.Evennement = studentregd.Evennement;




        }


        // DELETE: api/RecWebApi/5
        public IHttpActionResult Delete(int id)

        {
            Seat comp = MyService.GetById(id);

            MyService.Delete(comp);
            MyService.Commit();

            return Ok(comp);


        }
    


}
}





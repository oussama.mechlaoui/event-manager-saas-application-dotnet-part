﻿using EM.Domain.Entities;
using EM.Service.Web_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Controllers
{
    public class TicketvipController : ApiController
    {

        IDashService MyService = null;


        private DashService ms = new DashService();
        List<EM.Presentation.Models.Ticket> reclams = new List<EM.Presentation.Models.Ticket>();
        public TicketvipController()
        {
            MyService = new DashService();
            Index();
            reclams = Index().ToList();
        }


        public List<EM.Presentation.Models.Ticket> Index()
        {
            List<Ticket> mandates = ms.getMandates();
            List<EM.Presentation.Models.Ticket> mandatesXml = new List<EM.Presentation.Models.Ticket>();
            foreach (Ticket i in mandates)
            {
                mandatesXml.Add(new EM.Presentation.Models.Ticket
                {
                     idTicket = i.idTicket,
                     prixTicket=i.prixTicket,
                     Evennement=i.Evennement,
                     DateLancementSales=i.DateLancementSales,
                     EventId=i.EventId,
                     TicketStatus=i.TicketStatus,
                 
                });
            }
            return mandatesXml;
        }

        // GET: api/RecWebApi
        [Route("api/Ticket")]
        public IEnumerable<EM.Presentation.Models.Ticket> Get()
        {
            return reclams;
        }




        public Ticket Get(int id)
        {
            Ticket comp = MyService.GetById(id);

            return comp;
        }




        // GET: api/RecWebApi/5
        /**   public string Get(int? id)
           {
               return null;
           }
       **/
        // POST: api/RecWebApi



        public EM.Presentation.Models.Ticket registerStudent(Ticket studentregd)
        {
            EM.Presentation.Models.Ticket stdregreply = new EM.Presentation.Models.Ticket();
            MyService.Add(studentregd);


            stdregreply.prixTicket = studentregd.prixTicket;

            stdregreply.TicketStatus = studentregd.TicketStatus;


            stdregreply.DateLancementSales = studentregd.DateLancementSales;
            stdregreply.Evennement = studentregd.Evennement;



            return stdregreply;
        }

        [HttpPost]
        [Route("api/Rec")]
        public Ticket Post(Ticket proj)
        {


            MyService.Add(proj);
            MyService.Commit();

            return proj;
        }


        // PUT: api/RecWebApi/5
        [HttpPut]
        [Route("api/RecUp")]
        public void Put(int id, Ticket studentregd)
        {
            Ticket stdregreply = MyService.GetById(id);
            stdregreply.prixTicket = studentregd.prixTicket;

            stdregreply.TicketStatus = studentregd.TicketStatus;


            stdregreply.DateLancementSales = studentregd.DateLancementSales;
            stdregreply.Evennement = studentregd.Evennement;
        }


        // DELETE: api/RecWebApi/5
        public IHttpActionResult Delete(int id)

        {
            Ticket comp = MyService.GetById(id);

            MyService.Delete(comp);
            MyService.Commit();

            return Ok(comp);


        }
    }








}

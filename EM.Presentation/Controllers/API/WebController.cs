﻿using EM.Service.Web_api;
using Service.Pattern;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Controllers
{
    public class WebController : ApiController
    {
        StatService ss = new StatService();
        IStatService iss = new StatService();
        // GET: api/RecWebApi
        [Route("api/statTicket")]

        public List<int> Get()
        {
            return ss.SommePrixTicket();
        }

    }


    public class TicketTypeController : ApiController
    {
        TService tt = new TService();
        ITService iss = new TService();
        // GET: api/RecWebApi

        [Route("api/TypeTicket")]

        public List<int> Get()
        {
            return tt.TypeTicket();
        }
    }
    //////////////////

    public class TicketStatusController : ApiController
    {
        KService tt = new KService();
        IKService iss = new KService();
        // GET: api/RecWebApi

        [Route("api/StatusTicket")]

        public ArrayList Get()
        {
            return tt.StatusTicket();
        }
    }


    //////////////////
    public class SeatTypeController : ApiController
    {
        SService tt = new SService();
        ISService iss = new SService();
        // GET: api/RecWebApi

        [Route("api/TypeSeat")]

        public List<int> Get()
        {
            return tt.TypeSeat();
        }
    }
    public class SeatConController : ApiController
    {
        SeatConService tt = new SeatConService();
        ISeatConService iss = new SeatConService();
        // GET: api/RecWebApi

        [Route("api/SeatCon")]

        public List<Double> Get()
        {
            return tt.TicketSum();
        }
    }
}

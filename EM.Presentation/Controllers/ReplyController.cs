﻿using EM.Data;
using EM.Domain.Entities;
using EM.Presentation.Areas.Tenant;
using EM.Presentation.Areas.Tenant.Helpers;
using EM.Presentation.Models;
using EM.Service;
using EM.Service.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Presentation.Controllers
{
    public class ReplyController : AppController
    {

        IUserService UserService;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private Tenant _tenant;
        IParticipantService MyParticipantService;
        ICommentService MyCommentService;
        IReplyService MyReplyService;

        public ReplyController()
        {
            UserService = new UserService();
            MyParticipantService = new ParticipantService();
            MyCommentService = new CommentService();
            MyReplyService = new ReplyService();
        }
        public ReplyController(ApplicationUserManager userManager, ApplicationRoleManager roleManager) : base()
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public Tenant Current_Tenant
        {
            get
            {
                return _tenant ?? base.current_tenant;
            }
            private set
            {
                _tenant = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        /************************************ Actions ************************************************************/



        // GET: Reply
        //Affichage
        public ActionResult Index1(string searchString)
        {
            var replies = new List<ReplyVM>();
            ApplicationUser current_user = UserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            foreach (Comment c in MyCommentService.SearchCommentsByName(searchString))
            {
                var participant = UserManager.FindById(c.ParticipantId);
                replies.Add(new ReplyVM()
                {
                    Id = c.Id,
                    Text = c.Text,
                    CreatedOn = c.CreatedOn,
                    //ParticipantId = c.ParticipantId,
                    ParticipantName = participant.UserName,
                    //ParticipantImage = participant.ParticipantImage,
                });
            }
            return View(replies);
        }

        // GET: Comment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Comment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comment/Create
        [HttpPost]
        [TenantAuthorize(Roles = "Participant")]
        public PartialViewResult Create1(string contenu)
        {
            ApplicationUser current_user = UserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            Reply ReplyDomain = new Reply()
            {

                Text = contenu,
                CreatedOn = DateTime.UtcNow,
                ParticipantId = current_user.Id,
                ParticipantName = current_user.UserName,
                //ParticipantImage = ParticipantImage.FileName,

            };
            MyReplyService.Add(ReplyDomain);
            MyReplyService.Commit();



            return PartialView("OneReply", new ReplyVM
            {
                Text = ReplyDomain.Text,
                CreatedOn = ReplyDomain.CreatedOn,
                ParticipantId = ReplyDomain.ParticipantId,
                ParticipantName = ReplyDomain.ParticipantName,
                //ParticipantImage = ReplyDomain.ParticipantImage,
            });
        }

        // GET: Comment/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Comment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index1");
            }
            catch
            {
                return View();
            }
        }

        // GET: Comment/Delete/5
        public ActionResult Delete1(int id)
        {
            return View();
        }

        // POST: Comment/Delete/5
        [HttpPost]
        public ActionResult Delete1(int id, FormCollection collection)
        {
            Reply r = MyReplyService.GetById(id);
            MyReplyService.Delete(r);
            MyReplyService.Commit();


            return RedirectToAction("Index1");

        }



        public ActionResult Edit3(int id)
        {

            Reply p = MyReplyService.GetById(id);
            ReplyVM pm = new ReplyVM();

            pm.ParticipantId = p.ParticipantId;
            pm.Text = p.Text;
            pm.CreatedOn = p.CreatedOn;
            return View(pm);
        }

        // POST: Resource/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit3(int id, ReplyVM pm)
        {

            try
            {

                Reply p = MyReplyService.GetById(id);

                p.ParticipantId = pm.ParticipantId;
                p.Text = pm.Text;
                p.CreatedOn = pm.CreatedOn;
                MyReplyService.Update(p);
                MyReplyService.Commit();


                // TODO: Add update logic here




                return RedirectToAction("Index1");
            }
            catch (Exception ex)
            {
                return View(pm);
            }
        }

    }
}
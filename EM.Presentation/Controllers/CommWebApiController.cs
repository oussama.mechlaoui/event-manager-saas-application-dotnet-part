﻿using EM.Domain.Entities;
using EM.Presentation.Models;
using EM.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.Presentation.Controllers
{
    public class CommWebApiController : ApiController
    {
        ICommentService MyService = null;


        private CommentService ms = new CommentService();
        List<CommentVM> reclams = new List<CommentVM>();
        public CommWebApiController()
        {
            MyService = new CommentService();
            Index();
            reclams = Index().ToList();
        }

        [HttpGet]
        [Route("api/comments")]
        public List<CommentVM> Index()
        {
            IEnumerable<Comment> mandates = ms.GetMany(null,null,u => u.Participant);
            List<CommentVM> mandatesXml = new List<CommentVM>();
            foreach (Comment c in mandates)
            {
                mandatesXml.Add(new CommentVM
                {
                    Id = c.Id,
                    Text = c.Text,
                    CreatedOn = c.CreatedOn,
                    ParticipantId = c.ParticipantId,
                    ParticipantName = c.ParticipantName,
                });
            }
            return mandatesXml;
        }
        public IEnumerable<CommentVM> Get()
        {
            return reclams;
        }

        // GET: api/RecWebApi/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]

        [Route("api/comments/create")]
        public Comment Post(Comment com)
        {
            com.CreatedOn = DateTime.UtcNow;
            MyService.Add(com);
            MyService.Commit();
            return com;
        }

        [HttpPut]
        [Route("api/comments/update")]

        public void Put(int id, Comment loo)
        {
            Comment comp = MyService.GetById(id);
            comp.Text = loo.Text;
        }


        [Route("api/comments/delete/{id}")]
        public IHttpActionResult Delete(int id)

        {
            Comment comp = MyService.GetById(id);

            MyService.Delete(comp);
            MyService.Commit();

            return Ok(comp);


        }



    }
}

$(function() {
    "use strict";

    //Simple line chart
    new Chartist.Line('#simple-line-chart', {
        labels: ['Janv', 'Fev', 'Mar', 'Apr', 'May', 'Juin', 'Juill', 'Aout', 'Sep', 'Oct', 'Nov', 'Dec'],
        series: [
            [1, 9, 7, 8, 5, 12, 5, 7, 8, 5, 12, 5],
            
        ]
    }, {
        fullWidth: true,
        axisY: {
            labelInterpolationFnc: function(value) {
                return (value * 1);
            }
        },
        chartPadding: {
            right: 40
        },
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });

    //Simple line chart
    new Chartist.Line('#simple-line-chart2', {
        labels: false,
        series: [
            [1, 9, 7, 8, 5, 12, 15, 7, 8, 5, 12, 15],
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            left: -20
        },
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });


    $('#circle').circleProgress({
        value: 1,
        size: 130,
        fill: {
            color: ["#e53632"]
        }
    });
    $('#circle1').circleProgress({
        value: 1,
        size: 130,
        fill: {
            color: ["#e53632"]
        }
    });

});
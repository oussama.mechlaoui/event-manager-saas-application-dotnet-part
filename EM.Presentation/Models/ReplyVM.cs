﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Presentation.Models
{
    public class ReplyVM
    {

        [Key]
        public int Id { get; set; }

        [Display(Name = "Réponse")]
        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }

        [Display(Name = "ParticpantId")]
        public string ParticipantId { get; set; }
        public IEnumerable<SelectListItem> Participants { get; set; }
        [Display(Name = "ParticipantEmail")]
        public string ParticipantName { get; set; }

        [Display(Name = "CommentId")]
        public int? CommentId { get; set; }
        public IEnumerable<SelectListItem> Comments { get; set; }
    }
}
﻿using EM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Data.Configurations
{
    public class EvennementConfiguration : EntityTypeConfiguration<Evenement>
    {
        public EvennementConfiguration()
        {
            HasMany<Ticket>(x => x.Tickets).WithRequired(y => y.Evennement).HasForeignKey(c => c.EventId);
            HasMany<Seat>(x => x.Seats).WithRequired(y => y.Evennement).HasForeignKey(c => c.EventId);
        }
    }
}

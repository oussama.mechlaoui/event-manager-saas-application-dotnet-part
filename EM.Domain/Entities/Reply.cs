﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Domain.Entities
{
   public class Reply
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }

        public DateTime CreatedOn { get; set; }
        public string ParticipantId { get; set; }
        public virtual User Participant { get; set; }
        [Required]
        public string ParticipantName { get; set; }
        public int CommentId { get; set; }
        [ForeignKey("CommentId")]
        public virtual Comment Comment { get; set; }
    }
}

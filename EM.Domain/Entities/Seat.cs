﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Domain.Entities
{
    public enum TypeSeat
    {
        full,
        empty
    }

    public enum Status
    {
        sold,
        unsold
    }

    public class Seat
    {
        [Key]
        public int idSeat { get; set; }
        public float prixSeat { get; set; }
        public Status SeatStatus { get; set; }
        public TypeSeat SeatType { get; set; }


        public Evenement Evennement { get; set; }
        public int EventId { get; set; }

    }
}

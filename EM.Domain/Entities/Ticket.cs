﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Domain.Entities
{
    public enum TypeTicket
    {
        vip,
        simple
    }

    public enum onlineTicket
    {
        online,
        normal
    }

    public class Ticket
    {
        [Key]
        public int idTicket { get; set; }
        public float prixTicket { get; set; }
        public string TicketStatus { get; set; }
        public TypeTicket TicketType { get; set; }

        public onlineTicket Ticketonline { get; set; }
        public DateTime DateLancementSales { get; set; }
        public Evenement Evennement { get; set; }
        public int EventId { get; set; }

    }
}

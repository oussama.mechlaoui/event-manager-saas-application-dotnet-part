﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Domain.Entities
{
    public class User : IdentityUser
    {
        public string ParticipantImage { get; set; }
        public virtual ICollection<Comment> comments { get; set; }
        public virtual ICollection<Reply> Replies { get; set; }
    }
}
